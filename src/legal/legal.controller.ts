import { Controller, Get, Post, Header } from '@nestjs/common';
import { PdfService } from '../pdf/pdf.service'

@Controller('legal')
export class LegalController {
    constructor(private readonly pdfService: PdfService) {}

    @Get()
    @Header('Content-Type', 'application/pdf')
    @Header('Content-Disposition', 'attachment; filename=print.pdf')
    async getDefaultPDF()
    {
        return this.pdfService.getDefaultPDF();
    }
}
