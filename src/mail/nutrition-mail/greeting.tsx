import * as React from 'react';
import { MjmlHead, MjmlTitle, MjmlPreview, MjmlBody, MjmlSection, MjmlColumn, MjmlText } from 'mjml-react'


interface GreetingProps {
}

export const Greeting: React.FunctionComponent<GreetingProps> = 
    (props) => {
        const textBoxStyle: React.CSSProperties = {
            textAlign: "center",  
            margin: "10px 0"
        };

        const highlightText: React.CSSProperties = {
            color: "#d52b1e", 
            fontSize: "16px"
        };

        const normalText: React.CSSProperties = {
            color: "#010101", 
            fontSize: "16px"
        };
        
        return (
            <MjmlSection 
                background-color="#ffffff" 
                background-repeat="repeat" 
                background-size="auto" 
                border="0px solid #ffffff" 
                padding-bottom="10px" 
                padding-left="0px" 
                padding-right="0px" 
                padding-top="10px" 
                padding="10px 0px 10px 0px" 
                text-align="center" 
                vertical-align="top">
                <MjmlColumn>
                    <MjmlText 
                        align="left" 
                        color="#000000" 
                        container-background-color="#ffffff" 
                        font-family="Arial, sans-serif" 
                        font-size="16px" 
                        letter-spacing="normal" 
                        padding-bottom="0px" 
                        padding-left="25px" 
                        padding-right="25px" 
                        padding-top="0px" 
                        padding="0px 25px 0px 25px">
                        <p 
                            className="text-build-content" 
                            style={textBoxStyle}>
                            <span 
                                style={highlightText}>
                                <b>Dragă invitat(ă),</b>
                            </span>
                        </p>
                        <p 
                            className="text-build-content" 
                            style={textBoxStyle}>
                            <span 
                                style={normalText}>
                                <b>În cadrul acestui email veți regăsi toate răspunsurile pe care le-ați selectat la InfoPoint-ul de Nutriție din cadrul evenimentului </b>
                            </span>
                            <span 
                                style={highlightText}>
                                <b>Lilly Oncology Days.</b>
                            </span>
                        </p>
                    </MjmlText>
                </MjmlColumn>
            </MjmlSection>
        )
    }
        

