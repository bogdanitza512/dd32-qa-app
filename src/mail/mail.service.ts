import { Injectable } from '@nestjs/common';
import { SendMailDTO } from 'src/SendMail.dto';
import * as nodemailer from 'nodemailer'
import SMTPConnection = require('nodemailer/lib/smtp-connection');
import Mail = require('nodemailer/lib/mailer');
import SMTPTransport = require('nodemailer/lib/smtp-transport');
import mjml2html = require('mjml');
import { renderNutritionMail } from './nutrition-mail/render-mail';

@Injectable()
export class MailService {

    readonly transporter = nodemailer.createTransport(
        {
            host: String(process.env.MAILBOX_HOST || 'smtp.ethereal.email'),
            port: Number(process.env.MAILBOX_PORT || 587),
            auth: {
                user: String(process.env.MAILBOX_USER || 'elwin49@ethereal.email'),
                pass: String(process.env.MAILBOX_PASS || '8xhgUb4QQ6xqxEyhjq')
            }
        }
    );

    public sendMail(sendMailDTO: SendMailDTO) {
        
        const {html, errors} = renderNutritionMail(sendMailDTO.selection);

        const mailOptions: Mail.Options = {
            from: `"Lilly Info Point" <${String(process.env.MAILBOX_USER || 'elwin49@ethereal.email')}>`,
            bcc: String(process.env.MAILBOX_USER || 'elwin49@ethereal.email'),
            to: sendMailDTO.email,
            subject: 'Informații nutriție Lilly Oncology Days, 21 Martie 2020', // Subject line
            html: html // html body
        };
        
        this.transporter.sendMail(mailOptions, (err, info) => {
            if (err) {
                console.log(err);
                return;
            }

            console.log('Message sent: %s', info.messageId);
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        });

    }
}
