"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var common_1 = require("@nestjs/common");
var SendMail_dto_1 = require("../SendMail.dto");
var nodemailer = require("nodemailer");
var mail_app_1 = require("./mail-app");
var MailService = /** @class */ (function () {
    function MailService() {
        this.transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'elwin49@ethereal.email',
                pass: '8xhgUb4QQ6xqxEyhjq'
            }
        });
    }
    MailService.prototype.sendMail = function (sendMailDTO) {
        console.log(mail_app_1.renderOutTestEmail());
        var mailOptions = {
            from: '"Fred Foo 👻" <foo@blurdybloop.com>',
            to: 'bar@blurdybloop.com, baz@blurdybloop.com',
            subject: 'Hello ✔',
            text: 'Hello world?',
            html: '<b>Hello world?</b>' // html body
        };
        this.transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                console.log(err);
                return;
            }
            console.log('Message sent: %s', info.messageId);
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        });
    };
    MailService = __decorate([
        common_1.Injectable()
    ], MailService);
    return MailService;
}());
exports.MailService = MailService;
