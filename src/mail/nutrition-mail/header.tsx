import * as React from 'react';
import { MjmlHead, MjmlTitle, MjmlPreview, MjmlStyle } from 'mjml-react'


interface HeaderProps {
    title: string,
    preview: string
}

export const Header: React.FunctionComponent<HeaderProps> = 
    (props) => {
        return (
            <MjmlHead>
                <MjmlTitle>{props.title}</MjmlTitle>
                <MjmlPreview>{props.preview}</MjmlPreview>
                <MjmlStyle inline={true}>
                    {
                        `
                        .round-corners {
                            border-radius: 5px;
                        }
                        `
                    }
                </MjmlStyle>
            </MjmlHead>
        )
    }
