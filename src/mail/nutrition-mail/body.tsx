import * as React from 'react';
import { MjmlHead, MjmlTitle, MjmlPreview, MjmlBody } from 'mjml-react'


interface BodyProps {

}

export const Body: React.FunctionComponent<BodyProps> = 
    (props) => {
        return (
            <MjmlBody 
                background-color="#f4f4f4" 
                font-family="Arial, sans-serif">
                {props.children}
            </MjmlBody>
        )
    }


