import { Injectable } from '@nestjs/common';
import * as puppeteer from 'puppeteer'

@Injectable()
export class PdfService {
    private baseUrl: string = 'file:///Users/bogdanitza/Documents/heroku/dd32-qa-app/views/frontend/index.html';
    public getDefaultPDF() {
        // throw new Error("Method not implemented.");
        (async () => {
            const browser = await puppeteer.launch({args: ['--allow-file-access-from-files']});
            const page = await browser.newPage();
            await page.goto(this.baseUrl);
            await page.pdf({path: 'print.pdf'});

            await browser.close();
          })();
          console.log('Request...');
          return "great";
    }
}
