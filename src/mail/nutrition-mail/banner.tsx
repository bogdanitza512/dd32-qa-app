import * as React from 'react';
import { MjmlSection, MjmlColumn, MjmlImage } from 'mjml-react'

interface BannerProps {
    src: string
}

export const Banner: React.FunctionComponent<BannerProps> = 
    (props) => {
        return (
            <MjmlSection 
                background-repeat="repeat" 
                background-size="auto" 
                padding-bottom="0px" 
                padding-top="0px" 
                padding="20px 0" 
                text-align="center" 
                vertical-align="top">
                <MjmlColumn>
                    <MjmlImage 
                        align="center" 
                        alt="" 
                        border-radius="px" 
                        border="none" 
                        height="auto" 
                        href="" 
                        padding-bottom="0px" 
                        padding-left="0px" 
                        padding-right="0px" 
                        padding-top="0px" 
                        padding="10px 25px" 
                        src={props.src} 
                        target="_blank" 
                        title="" 
                        width="1200px">
                    </MjmlImage>
                </MjmlColumn>
            </MjmlSection>
        )
    }


