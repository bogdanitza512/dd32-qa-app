import { Controller, Post, Body } from '@nestjs/common';
import { MailService } from 'src/mail/mail.service';
import { SendMailDTO } from 'src/SendMail.dto';

@Controller('nutrition')
export class NutritionController {

    constructor(private readonly mailService: MailService) {}

    @Post()
    async sendMail(@Body() sendMailDTO: SendMailDTO)
    {
        this.mailService.sendMail(sendMailDTO);
        //console.log(sendMailDTO);
        return sendMailDTO;
    }

}
