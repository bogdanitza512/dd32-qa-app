"use strict";
exports.__esModule = true;
var React = require("react");
var mjml_react_1 = require("mjml-react");
function renderOutTestEmail() {
    // $ExpectType { html: string; errors: Error[]; }
    var result = mjml_react_1.render((<mjml_react_1.Mjml>
            <mjml_react_1.MjmlHead>
                <mjml_react_1.MjmlTitle>Last Minute Offer</mjml_react_1.MjmlTitle>
                <mjml_react_1.MjmlPreview>Last Minute Offer...</mjml_react_1.MjmlPreview>
            </mjml_react_1.MjmlHead>

            <mjml_react_1.MjmlBody width={500}>
                <mjml_react_1.MjmlSection fullWidth backgroundColor="#efefef">
                    <mjml_react_1.MjmlColumn>
                        <mjml_react_1.MjmlImage src="https://static.wixstatic.com/media/5cb24728abef45dabebe7edc1d97ddd2.jpg"/>
                    </mjml_react_1.MjmlColumn>
                </mjml_react_1.MjmlSection>
                <mjml_react_1.MjmlSection>
                    <mjml_react_1.MjmlColumn>
                        <mjml_react_1.MjmlButton padding="20px" backgroundColor="#346DB7" href="https://www.wix.com/">
                            I like it!
                    </mjml_react_1.MjmlButton>
                    </mjml_react_1.MjmlColumn>
                </mjml_react_1.MjmlSection>
            </mjml_react_1.MjmlBody>
        </mjml_react_1.Mjml>), { validationLevel: 'soft' });
}
exports.renderOutTestEmail = renderOutTestEmail;
