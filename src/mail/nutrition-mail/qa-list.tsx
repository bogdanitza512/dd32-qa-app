import * as React from 'react';
import { QASection } from './qa-section';

interface QAListProps {
    content: Array<number>
}

export const QAList: React.FunctionComponent<QAListProps> = 
    (props) => {
        const content = props.content;
        const items = content.map((id) => <QASection key={id} id={id}/>);

        return (
            <>
                {items}
            </>
        )
    }


