import * as React from 'react';
import { MjmlSection, MjmlColumn, MjmlText, MjmlSpacer } from 'mjml-react'

interface FooterProps {

}

export const Footer: React.FunctionComponent<FooterProps> = 
    (props) => {
        const textBoxStyle : React.CSSProperties = {
            margin: "10px 0"
        };

        return (
            <>
                <MjmlSection 
                    background-color="#ffffff" 
                    background-repeat="repeat" 
                    background-size="auto" 
                    border="0px solid #ffffff" 
                    padding-left="0px" 
                    padding-right="0px" 
                    padding-top="10px" 
                    padding="10px 0px 0px 0px" 
                    text-align="center" 
                    vertical-align="top">
                    <MjmlColumn>
                        <MjmlText 
                            align="left" 
                            color="#000000" 
                            container-background-color="#e0dedb" 
                            font-family="Arial, sans-serif" 
                            font-size="14px" 
                            letter-spacing="normal" 
                            padding-bottom="0px" 
                            padding-left="25px" 
                            padding-right="25px" 
                            padding-top="0px" 
                            padding="0px 25px 0px 25px">
                            <p
                                className="text-build-content" 
                                style={textBoxStyle}>
                                <b>
                                    PP-RB-RO-0211
                                </b>
                            </p>
                            <p 
                                className="text-build-content" 
                                style={textBoxStyle}>
                                Acest material promoţional este destinat profesioniștilor din domeniul sãnãtãţii.
                            </p>
                        </MjmlText>
                        <MjmlSpacer 
                            container-background-color="#ccc8c3" 
                            height="15px">
                        </MjmlSpacer>
                    </MjmlColumn>
                </MjmlSection>
            </>
        )
    }


