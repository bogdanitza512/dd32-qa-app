import * as React from 'react';
import { Mjml } from 'mjml-react'

interface MailProps {
    
}

export const Mail: React.FC<MailProps> =
    (props) => {
        return (
            <Mjml 
                owa="desktop" 
                lang="4.5.1">
                {props.children}
            </Mjml>
        )
    }
