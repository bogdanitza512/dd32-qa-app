import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LegalController } from './legal/legal.controller';
import { PdfService } from './pdf/pdf.service';
import { NutritionController } from './nutrition/nutrition.controller';
import { MailService } from './mail/mail.service';

@Module({
  imports: [],
  controllers: [AppController, LegalController, NutritionController],
  providers: [AppService, PdfService, MailService],
})
export class AppModule {}
