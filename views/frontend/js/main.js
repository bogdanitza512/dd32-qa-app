let openNavClick = document.querySelector('.nav-open-slider');
let closeNavClick = document.querySelector('.nav-close-button');
let navSliderSection = document.querySelector('.nav-slider-div');

openNavClick.onclick = openNavSlider;
closeNavClick.onclick = closeNavSlider;

function openNavSlider () {
    navSliderSection.classList.remove('on-close-nav');
    navSliderSection.classList.add('on-open-nav');
};

function closeNavSlider () {
    navSliderSection.classList.remove('on-open-nav');
    navSliderSection.classList.add('on-close-nav');
}

let navLinks = document.querySelectorAll('.nav-link');
navLinks.forEach((link) => {
    link.onclick = closeNavSlider;
})

let tabheaders = document.querySelectorAll('.tab-header');
tabheaders.forEach((btn) => {
    btn.onclick = changeTab;
});

function changeTab() {
    let tabNumber = parseInt(this.dataset.tab);

    document.querySelector('.active-tab').classList.remove('active-tab');
    this.classList.add('active-tab');

    let currentTab = document.querySelector('.show-tab');
    currentTab.classList.remove('show-tab');
    currentTab.classList.add('hide-tab');

    let newTabId = 'tab-div-' + tabNumber.toString().padStart(2,'0');
    let newTab = document.getElementById(newTabId);
    newTab.classList.remove('hide-tab');
    newTab.classList.add('show-tab');
}

var app = new function() {
    this.el = document.getElementById('custom-order');
    
    this.order = [
        {
            name : "Burger",
            qty : "1"
        },
        {
            name : "Fries",
            qty : "2"
        },
        {
            name : "Cola",
            qty : "-1"
        }
    ]
    this.UpdateCount = function(count) {
        var el   = document.getElementById('item-counter');
        var name = 'item';
        if (count) {
            if (count > 1) {
            name = 'items';
            }
            el.innerHTML = count + ' ' + name ;
        } else {
            el.innerHTML = 'No items';
        }
    };
    this.UpdateView = function() {
        var data = '';
        if (this.order.length > 0) {
            for (i = 0; i < this.order.length; i++) {
                data += '<tr>';
                data += '<td><p style="text-align: center">' + this.order[i].name + '</p></td>';
                data += '<td><p style="text-align: center">' + this.order[i].qty + '</p></td>';
                data += '<td><button onclick="app.Edit(' + i + ')">Edit</button></td>';
                data += '<td><button onclick="app.Delete(' + i + ')">Delete</button></td>';
                data += '</tr>';
            }
        }
        this.UpdateCount(this.order.length);
        return this.el.innerHTML = data;
    };
    this.Add = function () {
        var name_input = document.getElementById('add-name');
        var qty_input = document.getElementById('add-qty');
        // Get the value
        var item = {
            name : name_input.value.trim(),
            qty : qty_input.value
        }
        if (item.name) {
            // Add the new value
            this.order.push(item);
            // Reset input values
            name_input.value = '';
            qty_input.value = '0';
            // Dislay the new list
            this.UpdateView();
        }
    };
    this.Edit = function (id) {
        var name_input = document.getElementById('edit-name');
        var qty_input = document.getElementById('edit-qty');
        // Display value in the field
        name_input.value = this.order[id].name;
        qty_input.value = this.order[id].qty;
        // Display fields
        this.ToggleEdit(true);

        document.getElementById('saveEdit').onsubmit = function() {
            // Get value
            var item = {
                name : name_input.value.trim(),
                qty : qty_input.value
            }
            if (item.name) {
                // Edit value
                app.order.splice(id, 1, item);
                // Display the new list
                app.UpdateView();
                // Hide fields
                app.ToggleEdit(false);
            }
        }
    };
    this.Delete = function (item) {
        // Delete the current row
        this.order.splice(item, 1);
        // Display the new list
        this.UpdateView();
    };

    this.ToggleEdit = function (visible) {
        switch (visible) {
            case true:
                document.getElementById('spoiler').style.display = 'block';
                break;
            case false:
                document.getElementById('spoiler').style.display = 'none';
                break;
            default:
                break;
        }
    }
    this.send_data_btn = document.getElementById('send-data');
    
    this.send_data_btn.onclick = () => {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "http://localhost:3000/send-order", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify({ data: this.order }));
    };

}

app.UpdateView();

var i18n = new function () {

    var french = document.getElementById('fr_click');
    var english = document.getElementById('en_click');
    var placeholder = document.getElementById('placeholder_click');
    var fr_txt = document.querySelectorAll('#fr');
    var en_txt = document.querySelectorAll('#en');
    var placeholder_txt = document.querySelectorAll('#placeholder');

    french.addEventListener('click', function() {
        swithLang(french,english);
    }, false);
    
    english.addEventListener('click', function() {
        swithLang(english,french);
    }, false);

    placeholder.addEventListener('click', function() {
        fetch('http://localhost:3000/placeholder')
            .then(response => response.json())
            .then(json => {
                placeholder_txt.forEach((element) => {
                    element.innerHTML = json.body;
                });
                hideAll(en_txt);
                hideAll(fr_txt);
                showAll(placeholder_txt);
            });
    }, false)

    function swithLang(langueOn,langueOff){
        if (!langueOn.classList.contains('current_lang')) {
            langueOn.classList.toggle('current_lang');
            langueOff.classList.toggle('current_lang');
        }
        hideAll(placeholder_txt);
        if(langueOn.innerHTML == 'Fr'){
            hideAll(en_txt);
            showAll(fr_txt);
            
        }
        else if(langueOn.innerHTML == 'En'){
            hideAll(fr_txt);
            showAll(en_txt);
        }
    }
    function showAll(txt){
        txt.forEach((element) => {
            element.style.display = 'block';
        });
    }

    function hideAll(txt){
        txt.forEach((element) => {
            element.style.display = 'none';
        });
    }
    function init(){
        swithLang(french,english);
        setInterval(() => {
            placeholder_txt.forEach((element)=>
            {
                var color = element.style.color;
                if(color == 'blue') {
                    element.style.color = 'red';
                }
                else {
                    element.style.color = 'blue';
                }
            })
        }, 500);
    }
    init();
}



//TODO: footer ifreame table valid