import * as React from 'react';
import { MjmlHead, MjmlTitle, MjmlPreview, MjmlBody, MjmlSection, MjmlColumn, MjmlText, MjmlSpacer } from 'mjml-react'

interface QASectionProps {
    id: number
}

interface QAContent {
    question: string,
    answear: string
}

export const QASection: React.FunctionComponent<QASectionProps> = 
    (props) => {

        const textBoxStyle : React.CSSProperties = {
            margin: "10px 0"
        };

        const questionStyle : React.CSSProperties = {
            color: "#ffffff"
        };

        const answearStyle : React.CSSProperties = {
            color: "#000000"
        };

        const data: Array<QAContent> = [
            {
                question: 
                `
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, Lorem ipsum dolor sit amet, consectetur adipiscing elit - lorem ipsum dolor sit amet, consectetur adipiscing elit?
                `,
                answear: 
                `
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. &nbsp
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.&nbsp
                `
            }
        ]

        return (
            <>
                <MjmlSection 
                    background-color="#ffffff" 
                    background-repeat="repeat" 
                    background-size="auto" 
                    border="0px solid #ffffff" 
                    padding-bottom="10px" 
                    padding-left="20px" 
                    padding-right="20px" 
                    padding-top="10px" 
                    padding="10px 20px 10px 20px" 
                    text-align="center" 
                    vertical-align="top">
                    <MjmlColumn>
                        <MjmlText 
                            align="left" 
                            color="#000000" 
                            container-background-color="#00abc7" 
                            font-family="Arial, sans-serif" 
                            font-size="14px" letter-spacing="normal" 
                            padding-bottom="0px" 
                            padding-left="15px" 
                            padding-right="15px" 
                            padding-top="0px" 
                            padding="0px 15px 0px 15px"
                            cssClass="round-corners">
                            <p 
                                className="text-build-content" 
                                style={textBoxStyle}>
                                <span 
                                    style={questionStyle}>
                                    <b>{data[0].question}</b>
                                </span>
                            </p>
                        </MjmlText>
                        <MjmlSpacer 
                            container-background-color="#ffffff" 
                            height="10px">
                        </MjmlSpacer>
                        <MjmlText 
                            align="left" 
                            color="#000000" 
                            container-background-color="#d5d2ca" 
                            font-family="Arial, sans-serif" 
                            font-size="14px" letter-spacing="normal" 
                            padding-bottom="0px" padding-left="15px" 
                            padding-right="15px" padding-top="0px" 
                            padding="0px 15px 0px 15px"
                            cssClass="round-corners">
                            <p 
                                className="text-build-content" 
                                style={textBoxStyle}>
                                <span 
                                    style={answearStyle}>
                                    {data[0].answear}
                                </span>
                            </p>
                        </MjmlText>
                    </MjmlColumn>
                </MjmlSection>
            </>
        )
    }
        

