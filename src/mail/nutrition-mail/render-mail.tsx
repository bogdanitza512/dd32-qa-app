import * as React from 'react';
import { render } from 'mjml-react';
import { Mail } from './mail';
import { Header } from './header';
import { Body } from './body';
import { Banner } from './banner';
import { Greeting } from './greeting';
import { QASection } from './qa-section';
import { Footer } from './footer';
import { QAList } from './qa-list';

export function renderNutritionMail(selection: Array<number>) {       
    return render((
        <Mail>
            <Header 
                title="Informații nutriție Lilly Oncology Days" 
                preview="În cadrul acestui email veți regăsi toate răspunsurile pe care le-ați selectat la InfoPoint-ul de Nutriție din cadrul evenimentului"/>
            <Body>
                <Banner src="https://xz3pj.mjt.lu/tplimg/xz3pj/b/xvyhk/lv5x.png"/>
                <Greeting/>
                <QAList content={selection}/>
                <Footer/>
            </Body>
        </Mail>
    ), { validationLevel: 'soft' });
}